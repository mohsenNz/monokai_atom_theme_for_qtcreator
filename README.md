# ScreenShots

<img src="/uploads/8c4032d6abac3ba8b196b3170e1fb968/Screenshot_from_2019-01-21_14-41-07.png" width="50%">

<img src="/uploads/9f71cd2e6b2c6dc25c52f157728e912f/Screenshot_from_2019-01-21_14-47-27.png" width="50%">

# Install

## Unix-Like

```
git clone https://gitlab.gnome.org/mohsenNz/monokai_atom_theme_for_qtcreator.git
cd monokai_atom_theme_for_qtcreator
cp monokai_atom.xml ~/.config/QtProject/qtcreator/styles/
```

## Windows

1- clone project

2- go to the directory

3- copy monokai_atom.xml to %APPDATA%\QtProject\qtcreator\styles

# Usage

now open qtcreator and choose Monokai_atom from -> Tools -> Options... -> Text Editor -> Color Scheme

